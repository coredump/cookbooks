# Testing docker shit

control 'test nginx config' do
  impact 1.0
  desc 'testing if nginx config and log dir exists'

  describe file('/etc/docker_nginx.conf') do
    it { should be_file }
    it { should exist }
  end

  describe file('/var/log/nginx') do
    it { should exist }
    it { should be_directory }
  end
end

# control 'test docker config' do
#   impact 1.0
#   desc 'check if the docker daemon is running and containers are correct'

#   describe docker.containers.where { name == 'nginx' } do
#     it { should exist }
#     it { should be_running }
#     its(:ports) { should eq '0.0.0.0:80->80/tcp' }
#   end

#   # %w(coredump.io isitcreepy core.eti.br buildlogs).each do |c|
#   #   describe docker_container(name: c) do
#   #     it { should exist }
#   #     it { should be_running }
#   #   end
#   # end
# end
