#
# Cookbook:: personal_server
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.
#

user 'coredump' do
  manage_home true
end

ssh_authorize_key 'coredump@coredump.io' do
  key 'AAAAB3NzaC1yc2EAAAADAQABAAABAQC97EsLDpsA4Q9KPF7KkkW4a+kSNA6pToK5XvED0IWzWtlQtJ/xIul4dQMzd5s9s5C91Q8tDrZmERbjmIV7IbsqePHvK31azCDOqB5LH0v5k7K7ATmGacA2CYz7t9aEISyjc6sukLrEvkXpD3hJq5xHu+xYUsWrSo/ir2i0heFHEhCwnyeMIf55/24x5WsQ0rq9i1h3q0gOLk4D0P4871w9sdcV5Ac8FJi9tBVlRUVrQlWcT2sUsl3hKhQow/l3ja/X4vjklexsuIdO/n8/x1p7Uuzj9oDt08o7m9+N7VenAhch6D6q76bwbSWIUNKu2wBloZt/aPVU3fFiWCjltqnd'
  user 'coredump'
end

# Directory resource
directory '/var/log/nginx/' do
  recursive true
  action :create
end

cookbook_file '/etc/docker_nginx.conf' do
  source 'docker_nginx.conf'
  action :create
  notifies :redeploy, 'docker_container[nginx]'
end

# Docker stuff

docker_service 'default' do
  action [:create, :start]
end

docker_network 'i_sites' do
  subnet '192.168.30.0/24'
  gateway '192.168.30.1'
  action :create
end

docker_registry 'registry.gitlab.com' do
  username node['glab_token_name']
  password node['glab_token_secret']
  email 'jose.junior@gmail.com'
end

docker_image 'nginx' do
  tag '1.7'
  notifies :redeploy, 'docker_container[nginx]'
end

docker_image 'coredump.io image' do
  repo 'registry.gitlab.com/coredump/coredump.io'
  tag 'master'
  notifies :redeploy, 'docker_container[coredump.io]'
  notifies :redeploy, 'docker_container[nginx]'
end

docker_image 'core.eti.br image' do
  repo 'registry.gitlab.com/coredump/core.eti.br'
  tag 'master'
  notifies :redeploy, 'docker_container[core.eti.br]'
  notifies :redeploy, 'docker_container[nginx]'
end

docker_image 'isitcreepy image' do
  repo 'registry.gitlab.com/coredump/isitcreepy'
  tag 'master'
  notifies :redeploy, 'docker_container[isitcreepy]'
  notifies :redeploy, 'docker_container[nginx]'
end

docker_image 'buildlogs image' do
  repo 'registry.gitlab.com/coredump/buildlogs'
  tag 'master'
  notifies :redeploy, 'docker_container[buildlogs]'
  notifies :redeploy, 'docker_container[nginx]'
end

docker_container 'isitcreepy' do
  repo 'registry.gitlab.com/coredump/isitcreepy'
  tag 'master'
  restart_policy 'always'
  kill_after 10
  network_mode 'i_sites'
end

docker_container 'coredump.io' do
  repo 'registry.gitlab.com/coredump/coredump.io'
  tag 'master'
  restart_policy 'always'
  kill_after 10
  volume '/usr/share/nginx/html'
end

docker_container 'core.eti.br' do
  repo 'registry.gitlab.com/coredump/core.eti.br'
  tag 'master'
  restart_policy 'always'
  kill_after 10
  volume '/usr/share/html/core.eti.br/'
end

docker_container 'buildlogs' do
  repo 'registry.gitlab.com/coredump/buildlogs'
  tag 'master'
  restart_policy 'always'
  kill_after 10
  volume '/usr/share/buildlogs'
end

docker_container 'nginx' do
  tag '1.7'
  port '80:80'
  network_mode 'i_sites'
  restart_policy 'always'
  volume %w(
    /etc/docker_nginx.conf:/etc/nginx/nginx.conf
    /var/log/nginx:/var/log/nginx
  )
  volumes_from %w(
    coredump.io
    buildlogs
    core.eti.br
  )
end

