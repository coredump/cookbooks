This deploys the stuff at

http://core.eti.br
http://coredump.io
http://coredump.io/buildlogs
http://isitcreepy.coredump.io

Command for that:

```
be knife solo cook 74.50.61.180 tmp.json  -N coredump.io --clean-up
```

Of course it only works if you have the access for that.

`tmp.json` is a json file with attributes for gitlab credentials and `run_list`
